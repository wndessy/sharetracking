from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
import math


import pandas as pd

def linearRegressionPrice():
    df = pd.read_csv('sasini2.csv')
    df=df.sort_values(by='Date')
    # dates=pd.to_datetime(df['Date']).values.reshape(-1, 1)
    df=df.drop('Date', axis=1)
    print df

    # print depVar1
    # X = df.drop('Date', axis=1)
    # y = pd.to_datetime(df['Date']).values.reshape(-1, 1)
    X = df.drop('Target', axis=1)
    y = df['Target']

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=1)
    print("My training dataset for is : \n===============================\n {}".format(X_train))
    print("My test dataset for is : \n===============================\n {}".format(X_test))

    # regression_model= LinearRegression(copy_X=True, fit_intercept=True, n_jobs=1, normalize=False)
    regression_model = LinearRegression()
    regression_model.fit(X_train, y_train)
    # printing coefficients
    print("The coefficient for each variables are : \n===============================\n")
    for idx, col_name in enumerate(X_train.columns):
        print(" {} is {}".format(col_name, regression_model.coef_[idx]))
   #Printing the intercepts
    intercept = regression_model.intercept_
    print("The intercept for my model is: \n===============================\n {}".format(intercept))
     # scoring the model
    regression_model.score(X_test, y_test)
    y_predict = regression_model.predict(X_test)
    regression_model_mse = mean_squared_error(y_predict, y_test)
    print("The  mean square error of my model is: \n===============================\n {}".format(regression_model_mse))
    math.sqrt(regression_model_mse)


    #Predicting a given share volume
    prediction=regression_model.predict([[26.5,26.5,25.5,26,1700,25.5]])
    print("My share Price Prediction is : \n===============================\n {}".format(prediction))

linearRegressionPrice()





