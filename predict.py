import csv
import numpy
from sklearn.svm import SVR
from  sklearn.linear_model import LinearRegression
import matplotlib.pyplot as pyplot
import time
import datetime

import pandas as pd



def getdata(filename):
    dates = []
    prices = []
    with open(filename,'r')as csvfile:
        # csvfile.readlines
        csvFileReader=csv.reader(csvfile)
        next(csvFileReader) #skip the first line
        date = 1
        for row in csvFileReader :
             strindDate= str(row[0].strip())
             date += 1
             dates.append(date) #apend day
             prices.append(row[5])# apend price

    predicted_prices = SuportVector(dates, prices)
    print predicted_prices
    return


def SuportVector(dates,prices):
    dates=numpy.reshape(dates,(len(dates),1))

    svr_len = SVR(kernel='linear', C=1e3)
    svr_len.fit(dates, prices)
    pred_linear=svr_len.predict(dates)
    pyplot.plot(dates,pred_linear,color='red' ,label='Linnear Model')

    svr_poly=SVR(kernel='poly',C=1e3,degree=2)
    svr_poly.fit(dates,prices)
    pred_poly=svr_poly.predict(dates)
    pyplot.plot(dates,pred_poly,color='blue',label='Polynomial model')

    svr_rbf=SVR(kernel='rbf',C=1e3,gamma=0.1)
    svr_rbf.fit(dates,prices)
    svr_rbf.predict(dates)
    pyplot.plot(dates,svr_rbf.predict(dates),color='green',label='RBF model')

    pyplot.scatter(dates, prices, color='blue', label='data')
    pyplot.xlabel("Date")
    pyplot.ylabel('Price')
    pyplot.title('Support Vector regression')
    pyplot.legend()
    # pyplot.show()
    return

def linearRegression(dates,OtherParameter):
    linearModel = LinearRegression()
    linearModelfit=linearModel.fit(dates,OtherParameter)
    linearModelPred=linearModel.predict(OtherParameter)
    pyplot.plot(dates, linearModel, color='red', label='Linnear Model')
    pyplot.scatter(dates, OtherParameter, color='blue', label='data')
    pyplot.xlabel("Date")
    pyplot.ylabel(OtherParameter)
    pyplot.title('Linnear Regression Models')
    pyplot.legend()
    pyplot.show()
    return

# getdata('sasini.csv')
    # return svr_rbf.predict(x)[0],svr_poly(x)[0],svr_len(x)[0]



def ts2string(ts, fmt="%d/%m/%Y"):
    dt = datetime.datetime.fromtimestamp(ts)
    return dt.strftime(fmt)

def string2ts(string, fmt="%d/%m/%Y"):
    dt = datetime.datetime.strptime(string, fmt)
    t_tuple = dt.timetuple()
    return int(time.mktime(t_tuple))


